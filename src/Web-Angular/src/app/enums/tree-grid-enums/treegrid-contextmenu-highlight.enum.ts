export enum TreeGreedContextMenuHighlightEnum {
    None = "none",
    Filter = "#E7E132",
    Multisort = "#E7E132",
    Multiselect = "#E7E132",
    Copy = "#DAF7A6",
    Cut = "#BCA09A"
  }