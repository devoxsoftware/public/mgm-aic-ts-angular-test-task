import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarModule } from '@syncfusion/ej2-angular-navigations';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TreeGridModule } from '@syncfusion/ej2-angular-treegrid';
import { CustomTreeGridComponent } from './components/custom-grid/custom-treegrid.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomTreeGridComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule, 
    ToolbarModule, 
    DialogModule,
    TreeGridModule,
    ReactiveFormsModule, 
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
