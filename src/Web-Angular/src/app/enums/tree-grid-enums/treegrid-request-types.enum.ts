export enum TreeGreedRequestTypesEnum {
    Add = "add",
    Cancel = "cancel",
    Save = "save"
}