import { Component, OnInit } from '@angular/core';
import { BaseTreeGridData } from './models/base-treegrid-data.model';
import { ColumnsMetadata } from './models/columns-metadata.model';
import { GridDataService } from './services/grid-data-service/grid-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {  
  public gridData: BaseTreeGridData[];
  public columnsMetadata: Array<ColumnsMetadata>;

  constructor(private gridDataService: GridDataService) {    
  }
  ngOnInit(): void {
    this.gridData = this.gridDataService.getTaskDataInfo();
    this.columnsMetadata = this.gridDataService.getColmnMetadata();
  };
}
