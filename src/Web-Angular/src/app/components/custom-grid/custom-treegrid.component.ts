import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ColumnMenuService, ContextMenuService, EditService, EditSettingsModel, FilterService, FilterSettingsModel, 
        ResizeService, RowDDService, SelectionService,SelectionSettingsModel, SortService, ToolbarItems,
        ToolbarService, TreeGridComponent } from '@syncfusion/ej2-angular-treegrid';  
import * as _ from 'lodash';
import { TreeGreedContextMenuHighlightEnum,  } from '../../enums/tree-grid-enums/treegrid-contextmenu-highlight.enum';
import { ContextMenuActionsEnum  } from '../../enums/tree-grid-enums/context-menu-actions.enum';
import { TreeGreedToolbarActionsEnum  } from '../../enums/tree-grid-enums/treegrid-toolbar-actions.enum';
import { TreeGreedSelectionTypesEnum  } from '../../enums//tree-grid-enums/treegrid-selection-types.enum';
import { TreeGreedRequestTypesEnum  } from '../../enums//tree-grid-enums/treegrid-request-types.enum';
import { BaseTreeGridData } from 'src/app/models/base-treegrid-data.model';
import { ColumnsMetadata } from 'src/app/models/columns-metadata.model';

@Component({
  selector: 'custom-treegrid',
  templateUrl: './custom-treegrid.component.html',
  styleUrls: ['./custom-treegrid.component.scss'],
  providers: [SortService, 
              EditService, 
              ContextMenuService, 
              FilterService, 
              ResizeService, 
              ToolbarService, 
              ColumnMenuService,
              RowDDService,
              SelectionService]
})
export class CustomTreeGridComponent implements OnInit {
  @ViewChild('treeGrid')
  public treeGrid: TreeGridComponent;
  @Input() columnsMetadata: Array<ColumnsMetadata>;
  @Input() data: BaseTreeGridData[];

  public allowFiltering: boolean = false;
  public allowMultiSorting: boolean = false;
  public contextMenuItems: Array<Object>;
  public editSettings: EditSettingsModel;
  public filterSettings: Object;
  public lastOperation: ContextMenuActionsEnum;
  public lastFreeTaskId: number;
  public menu: any[];
  public recordJustAdded: boolean = false;
  public selectionOptions: SelectionSettingsModel;
  public selectedRows: any;
  public selectedRecords: any;
  public toolbar: ToolbarItems[];

  ngOnInit(): void {
    this.contextMenuItems= [
      { text: 'Filter', target: '.e-headercontent', id: ContextMenuActionsEnum.Filter },
      { text: 'Multi-Sort', target: '.e-headercontent', id: ContextMenuActionsEnum.MultiSort },
      { text: 'Multi-Select', target: '.e-headercontent', id: ContextMenuActionsEnum.MultiSelect },
      { text: 'Copy Rows', target: '.e-content', id: ContextMenuActionsEnum.CopyRow },
      { text: 'Сut Rows', target: '.e-content', id: ContextMenuActionsEnum.CutRow },
      { text: 'Paste Rows', target: '.e-content', id: ContextMenuActionsEnum.PasteRow },
      'Copy'
    ];
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, mode: "Dialog" };
    this.filterSettings = { type: 'FilterBar', hierarchyMode: 'Parent', mode: 'Immediate' };
    this.lastFreeTaskId = this.getLastFreeTaskId();
    this.menu = ['ColumnChooser'];  
    this.selectionOptions = { type: TreeGreedSelectionTypesEnum.Single };
    this.toolbar = [TreeGreedToolbarActionsEnum.Add,
                    TreeGreedToolbarActionsEnum.Edit,
                    TreeGreedToolbarActionsEnum.Delete];
  }

  actionBegin(event: any): void {
    switch (event.requestType) {
      case TreeGreedRequestTypesEnum.Add: {
        this.recordJustAdded = true;

        break;
      }
      case TreeGreedRequestTypesEnum.Cancel: {
        this.recordJustAdded = false;

        break;
      }
      case TreeGreedRequestTypesEnum.Save: {
        if(this.recordJustAdded) {    
          event.data.id = this.lastFreeTaskId;
          this.recordJustAdded = false;
        }

        break;
      }
    }
  }

  contextMenuClick(event: any): void {
    switch(event.item.id) {
      case ContextMenuActionsEnum.Filter: {
        this.allowFiltering = !this.allowFiltering;
        (document.querySelector('#filter') as HTMLElement).style.background = this.allowFiltering 
                                                                              ? TreeGreedContextMenuHighlightEnum.Filter 
                                                                              : TreeGreedContextMenuHighlightEnum.None;
        break;
      }
      case ContextMenuActionsEnum.MultiSort: {
        this.allowMultiSorting = !this.allowMultiSorting;
        (document.querySelector('#multisort') as HTMLElement).style.background = this.allowMultiSorting 
                                                                              ? TreeGreedContextMenuHighlightEnum.Multisort 
                                                                              : TreeGreedContextMenuHighlightEnum.None;
        break;
      }
      case ContextMenuActionsEnum.MultiSelect: {
        this.selectionOptions.type = this.selectionOptions.type === TreeGreedSelectionTypesEnum.Multiple
                                    ? TreeGreedSelectionTypesEnum.Single 
                                    : TreeGreedSelectionTypesEnum.Multiple;
        this.treeGrid.selectionSettings = this.selectionOptions;
        (document.querySelector('#multiselect') as HTMLElement).style.background = this.selectionOptions.type === TreeGreedSelectionTypesEnum.Multiple 
                                                                                  ? TreeGreedContextMenuHighlightEnum.Multiselect 
                                                                                  : TreeGreedContextMenuHighlightEnum.None;
        break;
      }
      case ContextMenuActionsEnum.CopyRow: {
        this.setSelectedRowsBackground(TreeGreedContextMenuHighlightEnum.None);

        this.selectedRecords = this.treeGrid.getSelectedRecords();
        this.selectedRows = this.treeGrid.getSelectedRows();

        this.setSelectedRowsBackground(TreeGreedContextMenuHighlightEnum.Copy);
        this.lastOperation = ContextMenuActionsEnum.CopyRow;

        break;
      }
      case ContextMenuActionsEnum.CutRow: {
        this.setSelectedRowsBackground(TreeGreedContextMenuHighlightEnum.None);

        this.selectedRecords = this.treeGrid.getSelectedRecords();
        this.selectedRows = this.treeGrid.getSelectedRows();

        this.setSelectedRowsBackground(TreeGreedContextMenuHighlightEnum.Cut);
        this.lastOperation = ContextMenuActionsEnum.CutRow;

        break;
      }
      case ContextMenuActionsEnum.PasteRow: {
        if (this.lastOperation === ContextMenuActionsEnum.CopyRow) {
          const result = this.flattenArray(this.data, []);
          const parentNodeForPaste = this.treeGrid.getSelectedRecords()[0];
          
          let filteredRecords = [];
          this.selectedRecords.forEach(record => {
            let indexOfRecord = this.selectedRecords.map(function(rec) {return rec.id; }).indexOf(record.parentItem?.id);
            if (indexOfRecord > -1) {
              return;
            }
            filteredRecords.push(_.cloneDeep(record));
          });

          filteredRecords.forEach(record => {
            let newElement = _.cloneDeep(result.find(node => node.id === record.id));
            this.insertCopyData(this.data, newElement, parentNodeForPaste['id']);
          })

          this.lastOperation = ContextMenuActionsEnum.CopyRow;
          this.treeGrid.refresh();
        } else if (this.lastOperation === ContextMenuActionsEnum.CutRow) {
          const parentNodeForPaste = this.treeGrid.getSelectedRecords()[0];

          this.selectedRecords.forEach(record => {
            let indexOfRecord = this.selectedRecords.map(function(rec) {return rec.id; }).indexOf(record.parentItem?.id);
            if (indexOfRecord > -1) {
              return;
            }

            const result = this.cutRows(this.data, record.id);
            this.insertCutData(this.data, result, parentNodeForPaste['id']);
          });

          this.lastOperation = ContextMenuActionsEnum.PasteRow;
          this.treeGrid.refresh();

          this.selectedRecords = [];
          this.selectedRows = [];
        }

        this.setSelectedRowsBackground(TreeGreedContextMenuHighlightEnum.None);
      }
    }
  }

  private flattenArray(data: Array<BaseTreeGridData>, sourceArray: Array<BaseTreeGridData>): Array<BaseTreeGridData> {    
    data.forEach(node => {
      sourceArray.push(node);
      if (node.subItems) {
        this.flattenArray (node.subItems, sourceArray)
      }
    })
    return sourceArray;
  }

  private getLastFreeTaskId(): number {
    let actualId = 0;
    let result = this.flattenArray(this.data, []);
    for (let i = 0; i < result.length; i++) {
        if (result[i].id > actualId) {
          actualId = result[i].id;          
        }
    }

    return actualId + 1;
  }

  private cutRows(data:Array<BaseTreeGridData>, elementId: number) {
    if(!data) return;

    for (let index = 0; index < data?.length; index++) {
      if (data[index].id == elementId) {
        let opResult = _.cloneDeep(data[index]);
        data.splice(index, 1);

        return opResult;
      }
      let found = this.cutRows(data[index]?.subItems, elementId);
      if (found) return found;
    }
  }

  private insertCutData(data:Array<BaseTreeGridData>, newElement: BaseTreeGridData, elementId: number): void {
    if(!data) return;

    const parentNodeIndex = data.map(function(node) {return node.id; }).indexOf(elementId);
    if(parentNodeIndex > -1) {
      if (data[parentNodeIndex].subItems) {
        data[parentNodeIndex].subItems.push(newElement);
      } else {
        data[parentNodeIndex].subItems = [];
        data[parentNodeIndex].subItems.push(newElement);
      }
      return;
    } else {
      data.forEach(node => {
        this.insertCutData(node.subItems, newElement, elementId);
      });  
    }
  }

  private insertCopyData(data:BaseTreeGridData[], newElement: BaseTreeGridData, elementId: number): void {
    if(!data) return;
    const parentNodeIndex = data.map(function(node) {return node.id; }).indexOf(elementId);

    if(parentNodeIndex > -1) {
      newElement.id = this.lastFreeTaskId;
      this.lastFreeTaskId++;
      this.setNewSubItemsIds(newElement);
      if (data[parentNodeIndex].subItems) {
        data[parentNodeIndex].subItems.push(newElement);
      } else {
        data[parentNodeIndex].subItems = [];
        data[parentNodeIndex].subItems.push(newElement);
      }
      return;
    } else {
      data.forEach(node => {
        this.insertCopyData(node.subItems, newElement, elementId);
      });  
    }
  }

  private setNewSubItemsIds(data:BaseTreeGridData): void {
    if(data.subItems) {
      data.subItems.forEach(node => {
        node.id = this.lastFreeTaskId;
        ++this.lastFreeTaskId;
        this.setNewSubItemsIds(node);
      });
    }
  }


  private setSelectedRowsBackground(color: string): void {
    this.selectedRows?.forEach(gridRow => {
      document.querySelectorAll('.e-row').forEach(rowElementHTML => {
        if(rowElementHTML['ariaRowIndex'] === gridRow['ariaRowIndex'])
        (rowElementHTML as HTMLElement).style.background = color;
      }); 
    });
  }
}
