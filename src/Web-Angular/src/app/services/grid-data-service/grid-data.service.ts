import { Injectable } from '@angular/core';
import { ColumnsMetadata } from 'src/app/models/columns-metadata.model';
import { TaskDataInfo } from 'src/app/models/task-data-info.model';
import { columnsMetadata, sampleData } from './grid-data';

@Injectable({
  providedIn: 'root'
})
export class GridDataService {
  public data: TaskDataInfo[];
  public columnsMetadata: Array<ColumnsMetadata>;

  constructor() {
    this.data = sampleData;
    this.columnsMetadata = columnsMetadata;
  }

  public getTaskDataInfo(): TaskDataInfo[] {
    return this.data;
  }

  public getColmnMetadata(): Array<any> {
    return this.columnsMetadata;
  }
}
