import { ColumnsMetadata } from "src/app/models/columns-metadata.model";

export let columnsMetadata: Array<ColumnsMetadata> = [
    {
        field:'taskName',
        headerText:'Task Name',
        textAlign:'Left',
        width:180
    },
    {
        field:'priority',
        headerText:'Priority',
        textAlign:'Right',
        width:90
    },
    {
        field:'approved',
        headerText:'Approved',
        textAlign:'Right',
        width:90
    },
    {
        field:'startDate',
        headerText:'Start Date',
        textAlign:'Right',
        width:90,
        format:'yMd'
    },
    {
        field:'endDate',
        headerText:'End Date',
        textAlign:'Right',
        width:90,
        format:'yMd'
    },
    {
        field:'duration',
        headerText:'Duration',
        textAlign:'Right',
        width:80
    }

];

export let sampleData: any = [
   {
       id: 1,
       taskName: 'Planning',
       startDate: new Date('02/03/2017'),
       endDate: new Date('02/07/2017'),
       progress: 100,
       duration: 5,
       priority: 'Normal',
       approved: false,
       isInExpandState: true,
       subItems: [
           { id: 2, taskName: 'Plan timeline', startDate: new Date('02/03/2017'), endDate: new Date('02/07/2017'), duration: 5, progress: 100, priority: 'Normal', approved: false },
           { id: 3, taskName: 'Plan budget', startDate: new Date('02/03/2017'), endDate: new Date('02/07/2017'), duration: 5, progress: 100, approved: true },
           { id: 4, taskName: 'Allocate resources', startDate: new Date('02/03/2017'), endDate: new Date('02/07/2017'), duration: 5, progress: 100, priority: 'Critical', approved: false },
           { id: 5, taskName: 'Planning complete', startDate: new Date('02/07/2017'), endDate: new Date('02/07/2017'), duration: 0, progress: 0, priority: 'Low', approved: true }
       ]
   },
   {
       id: 6,
       taskName: 'Design',
       startDate: new Date('02/10/2017'),
       endDate: new Date('02/14/2017'),
       duration: 3,
       progress: 86,
       priority: 'High',
       isInExpandState: false,
       approved: false,
       subItems: [
           { id: 7, taskName: 'Software Specification', startDate: new Date('02/10/2017'), endDate: new Date('02/12/2017'), duration: 3, progress: 60, priority: 'Normal', approved: false },
           { id: 8, taskName: 'Develop prototype', startDate: new Date('02/10/2017'), endDate: new Date('02/12/2017'), duration: 3, progress: 100, priority: 'Critical', approved: false },
           { id: 9, taskName: 'Get approval from customer', startDate: new Date('02/13/2017'), endDate: new Date('02/14/2017'), duration: 2, progress: 100, approved: true },
           { id: 10, taskName: 'Design Documentation', startDate: new Date('02/13/2017'), endDate: new Date('02/14/2017'), duration: 2, progress: 100, approved: true },
           { id: 11, taskName: 'Design complete', startDate: new Date('02/14/2017'), endDate: new Date('02/14/2017'), duration: 0, progress: 0, priority: 'Normal', approved: true }
       ]
   },
   {
       id: 12,
       taskName: 'Implementation Phase',
       startDate: new Date('02/17/2017'),
       endDate: new Date('02/27/2017'),
       priority: 'Normal',
       approved: false,
       duration: 11,
       subItems: [
           {
               id: 13,
               taskName: 'Phase 1',
               startDate: new Date('02/17/2017'),
               endDate: new Date('02/27/2017'),
               priority: 'High',
               approved: false,
               duration: 11,
               subItems: [{
                   id: 14,
                   taskName: 'Implementation Module 1',
                   startDate: new Date('02/17/2017'),
                   endDate: new Date('02/27/2017'),
                   priority: 'Normal',
                   duration: 11,
                   approved: false,
                   subItems: [
                       { id: 15, taskName: 'Development Task 1', startDate: new Date('02/17/2017'), endDate: new Date('02/19/2017'), duration: 3, progress: '50', priority: 'High', approved: false },
                       { id: 16, taskName: 'Development Task 2', startDate: new Date('02/17/2017'), endDate: new Date('02/19/2017'), duration: 3, progress: '50', priority: 'Low', approved: true },
                       { id: 17, taskName: 'Testing', startDate: new Date('02/20/2017'), endDate: new Date('02/21/2017'), duration: 2, progress: '0', priority: 'Normal', approved: true },
                       { id: 18, taskName: 'Bug fix', startDate: new Date('02/24/2017'), endDate: new Date('02/25/2017'), duration: 2, progress: '0', priority: 'Critical', approved: false },
                       { id: 19, taskName: 'Customer review meeting', startDate: new Date('02/26/2017'), endDate: new Date('02/27/2017'), duration: 2, progress: '0', priority: 'High', approved: false },
                       { id: 20, taskName: 'Phase 1 complete', startDate: new Date('02/27/2017'), endDate: new Date('02/27/2017'), duration: 0, priority: 'Low', approved: true }

                   ]
               }]
           },
           {
               id: 21,
               taskName: 'Phase 2',
               startDate: new Date('02/17/2017'),
               endDate: new Date('02/28/2017'),
               priority: 'High',
               approved: false,
               duration: 12,
               subItems: [{
                   id: 22,
                   taskName: 'Implementation Module 2',
                   startDate: new Date('02/17/2017'),
                   endDate: new Date('02/28/2017'),
                   priority: 'Critical',
                   approved: false,
                   duration: 12,
                   subItems: [
                       { id: 23, taskName: 'Development Task 1', startDate: new Date('02/17/2017'), endDate: new Date('02/20/2017'), duration: 4, progress: '50', priority: 'Normal', approved: true },
                       { id: 24, taskName: 'Development Task 2', startDate: new Date('02/17/2017'), endDate: new Date('02/20/2017'), duration: 4, progress: '50', priority: 'Critical', approved: true },
                       { id: 25, taskName: 'Testing', startDate: new Date('02/21/2017'), endDate: new Date('02/24/2017'), duration: 2, progress: '0', priority: 'High', approved: false },
                       { id: 26, taskName: 'Bug fix', startDate: new Date('02/25/2017'), endDate: new Date('02/26/2017'), duration: 2, progress: '0', priority: 'Low', approved: false },
                       { id: 27, taskName: 'Customer review meeting', startDate: new Date('02/27/2017'), endDate: new Date('02/28/2017'), duration: 2, progress: '0', priority: 'Critical', approved: true },
                       { id: 28, taskName: 'Phase 2 complete', startDate: new Date('02/28/2017'), endDate: new Date('02/28/2017'), duration: 0, priority: 'Normal', approved: false }

                   ]
               }]
           },
           {
               id: 29,
               taskName: 'Phase 3',
               startDate: new Date('02/17/2017'),
               endDate: new Date('02/27/2017'),
               priority: 'Normal',
               approved: false,
               duration: 11,
               subItems: [{
                   id: 30,
                   taskName: 'Implementation Module 3',
                   startDate: new Date('02/17/2017'),
                   endDate: new Date('02/27/2017'),
                   priority: 'High',
                   approved: false,
                   duration: 11,
                   subItems: [
                       { id: 31, taskName: 'Development Task 1', startDate: new Date('02/17/2017'), endDate: new Date('02/19/2017'), duration: 3, progress: '50', priority: 'Low', approved: true },
                       { id: 32, taskName: 'Development Task 2', startDate: new Date('02/17/2017'), endDate: new Date('02/19/2017'), duration: 3, progress: '50', priority: 'Normal', approved: false },
                       { id: 33, taskName: 'Testing', startDate: new Date('02/20/2017'), endDate: new Date('02/21/2017'), duration: 2, progress: '0', priority: 'Critical', approved: true },
                       { id: 34, taskName: 'Bug fix', startDate: new Date('02/24/2017'), endDate: new Date('02/25/2017'), duration: 2, progress: '0', priority: 'High', approved: false },
                       { id: 35, taskName: 'Customer review meeting', startDate: new Date('02/26/2017'), endDate: new Date('02/27/2017'), duration: 2, progress: '0', priority: 'Normal', approved: true },
                       { id: 36, taskName: 'Phase 3 complete', startDate: new Date('02/27/2017'), endDate: new Date('02/27/2017'), duration: 0, priority: 'Critical', approved: false },
                   ]
               }]
           }
       ]
   }
];
