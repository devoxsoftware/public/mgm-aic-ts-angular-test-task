export interface ColumnsMetadata {
    field: string;
    headerText: string;
    textAlign?: string;
    width?: number;
    format?: string;
}