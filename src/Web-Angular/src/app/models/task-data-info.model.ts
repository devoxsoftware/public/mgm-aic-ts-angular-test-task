import { BaseTreeGridData } from "./base-treegrid-data.model";

export interface TaskDataInfo extends BaseTreeGridData {
    approved: boolean;
    duration: number;
    endDate: Date;
    isInExpandState?: boolean;
    priority?: string;
    progress?: string | number;
    startDate: Date;
    taskName: string;
}