export enum ContextMenuActionsEnum {
    CopyRow = "copyRow",
    CutRow = "cutRow",
    PasteRow = "pastRow",
    Filter = "filter",
    MultiSort = "multisort",
    MultiSelect = "multiselect"
  }