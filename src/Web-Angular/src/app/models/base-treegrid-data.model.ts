export interface BaseTreeGridData {
    id: number;
    subItems?: Array<BaseTreeGridData>;
}